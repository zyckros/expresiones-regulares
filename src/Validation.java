import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Validation {


    /**
     * Esta funcion acepta una string por parametro que sera un nombre simple o compuesto y devuelve true o false si esta correcto
     * @param name - String con el valor del nombre
     * @return
     */
    public static boolean validateName(String name) {

        if (name.length() > 20) {
            System.out.println("Nombre superior a 20 caracteres, vuelva a intentarlo");
            return false;
        }

        String newName = "";
        Pattern nombre = Pattern.compile("^[A-Z-áÁéÉíÍóÓúÚñÑ]?[a-z-áÁéÉíÍóÓúÚñÑ]{1,}(\\s*)([A-Z-áÁéÉíÍóÓúÚñÑ]?[a-z-áÁéÉíÍóÓúÚñÑ]{1,})*$");
        Matcher m = nombre.matcher(name);

        //pone el primer caracter en mayuscula
        newName = newName + name.substring(0, 1).toUpperCase();
        newName = newName + name.substring(1).toLowerCase();
        newName = newName.replaceAll("\\s+", " ");
        if (name.length() < 20) {
            name = String.format("%-20s", name);
        }

        System.out.println("Despues de modificar " + newName);
        System.out.println("longitud de name = " + name.length());


        if (m.matches()) {
            return true;
        } else {
            return false;
        }


    }

    /**
     * Esta funcion acepta una String por parametro que sera el apellido y devuelve true o false si es correcto
     * @param lastname1 - String con el valor del apellido
     * @return
     */
    public static boolean validationFirstLastname(String lastname1) {


        if (lastname1.length() > 30) {
            System.out.println("Nombre superior a 20 caracteres, vuelva a intentarlo");
            return false;
        }

        String newlastname1 = "";
        Pattern lastname = Pattern.compile("^[A-Z-áÁéÉíÍóÓúÚñÑ]?[a-z-áÁéÉíÍóÓúÚñÑ]{1,}(\\s*)([A-Z-áÁéÉíÍóÓúÚñÑ]?[a-z-áÁéÉíÍóÓúÚñÑ]{1,})*$");
        Matcher m = lastname.matcher(lastname1);

        //pone el primer caracter en mayuscula
        newlastname1 = newlastname1 + lastname1.substring(0, 1).toUpperCase();
        newlastname1 = newlastname1 + lastname1.substring(1).toLowerCase();
        newlastname1 = newlastname1.replaceAll("\\s+", " ");
        if (lastname1.length() < 20) {
            lastname1 = String.format("%-30s", lastname1);
        }

        System.out.println("Despues de modificar " + newlastname1);
        System.out.println("longitud de name = " + lastname1.length());


        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Esta funcion acepta una String por parametro que sera el DNI y devuelve true o false si es correcto o falso
     * @param dni - String con el valor del dni
     * @return
     */
    public static boolean validationDni(String dni) {

        String newDni = "";
        Pattern validateDni = Pattern.compile("^[0-9]{8}[a-z A-Z]$");
        Matcher m = validateDni.matcher(dni);

        if (m.matches()) {
            newDni = dni.substring(0, 8);
            newDni = newDni + dni.substring(dni.length() - 1).toUpperCase();
            System.out.println("new Dni" + newDni);
            return true;
        } else {
            System.out.println("Dni incorrecto, vuelva a intentarlo");
            return false;
        }
    }

    /**
     * Esta funcion recibe una string por parametro y devovlera true o false si el curso es correcto o no.
     * @param curso - String con el valor del curso
     * @return
     */

    public static boolean validationCourse(String curso) {

        Pattern validationCourse = Pattern.compile("^[1-3]{1}[a-z A-Z]{3}[-][n-t-m N-T-M]$");
        Matcher m = validationCourse.matcher(curso);
        if (m.matches()) {
            curso = curso.toUpperCase();
            System.out.println(curso);
            return true;
        } else {
            return false;
        }

    }

    /**
     * Esta funcion recibe un float por parametro y devolvera true o false segun sea o no correcto el parametro
     * @param tasa
     * @return
     */
    public static boolean validationTasa(float tasa) {
        if (tasa < 0 || tasa > 200) {
            System.out.println("Valor fuera de rango, vuelva a intentarlo");
            return false;
        }else{

            System.out.printf("%.2f", tasa);
            return true;
        }


    }
}
