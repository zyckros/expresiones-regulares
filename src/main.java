import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String name = "";
        String lastName1 = "";
        String lastName2 = "";
        String dni = "";
        String curso = "";
        float tasa = 0;
        boolean validation = false;


        /**
         * validacion del nombre
         */
        do {
            try {
                System.out.println("Introduce un nombre: ");
                name = scan.nextLine();
                validation = Validation.validateName(name);
            } catch (Exception e) {
                scan.nextLine();
                name = "";
            }

            if (!validation) {
                System.out.println("Datos incorrectos. Vuelva a intentarlo");
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);


        validation = false;


        /**
         * validacion primer apellido
         */

        do {
            try {
                System.out.println("Introduce el primer apellido: ");
                lastName1 = scan.nextLine();
                validation = Validation.validationFirstLastname(lastName1);
            } catch (Exception e) {
                scan.nextLine();
                lastName1 = "";
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);


        validation = false;

        /**
         * validacion segundo apellido
         */

        do {
            try {
                System.out.println("Introduce el segundo apellido: ");
                lastName2 = scan.nextLine();
                validation = Validation.validationFirstLastname(lastName2);
            } catch (Exception e) {
                scan.nextLine();
                lastName2 = "";
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);


        validation = false;

        /**
         * validacion DNI
         */

        do {
            try {
                System.out.println("Introduce el DNI: ");
                dni = scan.nextLine();
                validation = Validation.validationDni(dni);
            } catch (Exception e) {
                scan.nextLine();
                dni = "";
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);


        validation = false;

        /**
         * validacion curso
         */

        do {
            try {
                System.out.println("Introduce el curso: ");
                curso = scan.nextLine();
                validation = Validation.validationCourse(curso);
            } catch (Exception e) {
                scan.nextLine();
                curso = "";
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);

        validation = false;


        /**
         * validacion matricula
         */

        do {
            try {
                System.out.println("Introduce la tasa de matricula: ");
                tasa = scan.nextFloat();
                validation = Validation.validationTasa(tasa);
            } catch (Exception e) {
                scan.nextLine();
                tasa = 0;
            }

            System.out.println(" value of validation is --> " + validation);
        } while (!validation);

        System.out.println(name + " " + lastName1 + " " + lastName2 + " " + dni + " " + curso + " " + tasa);
    }

}
